import React from "react";
import ReactDOM from "react-dom";
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import { Provider } from "react-redux";
import store from "./redux/store/store";
import Fulllayout from "./layouts/fulllayout.jsx";

import "./assets/scss/style.css";
import axios from "axios";

axios.defaults.baseURL = `${process.env.REACT_APP_BASE_URL}`;
axios.defaults.headers.common["Authorization"] =
  localStorage.getItem(`${process.env.REACT_APP_APP_NAME}-token`) || null;
axios.defaults.headers.post["Content-Type"] = "application/json";

// axios.interceptors.request.use(
//   (request) => {
//     console.log(request);
//     // Edit request config
//     return request;
//   },
//   (error) => {
//     console.log(error);
//     return Promise.reject(error);
//   }
// );

// axios.interceptors.response.use(
//   (response) => {
//     console.log(response);
//     // Edit response config
//     return response;
//   },
//   (error) => {
//     console.log(error);
//     return Promise.reject(error);
//   }
// );

ReactDOM.render(
  <Provider store={store}>
    <Router>
      <Switch>
        <Route path="/" component={(props) => <Fulllayout {...props} />} />
      </Switch>
    </Router>
  </Provider>,
  document.getElementById("root")
);
