import React from "react";

const New = (props) =>
  props.show ? (
    <div
      style={{
        position: "absolute",
        top: "0",
        right: "0",
        bottom: "0",
        left: "0",
        backgroundColor: "rgba(0, 0, 0, 0.9)",
      }}
    >
      <div
        onClick={() => props.onClickHandle(false)}
        className="btn btn-danger right absolute"
      >
        <i className="fa fa-close"></i>
      </div>

      {props.children}
    </div>
  ) : null;

export default New;
