import React from "react";
import { Row, Form, Col, Button } from "react-bootstrap";

class AddNew extends React.Component {
  state = {};
  constructor(props) {
    super(props);

    this.initialState = {};
    const { schema } = this.props;

    Object.keys(this.props.schema).forEach((key) => {
      if (key && !schema[key].exclude)
        this.initialState[key] = schema[key].value;
    });

    this.state = this.initialState;
  }

  handleChange = (event) => {
    const { name, value } = event.target;

    this.setState({
      [name]: value,
    });
  };

  handleSubmit = (event) => {
    event.preventDefault();
    this.props.onFormSubmit(this.state);
    this.setState(this.initialState);
  };

  ListField = () => {
    const formField = [];
    const { schema, operation, entity } = this.props;

    Object.keys(schema).forEach((key) => {
      if (schema[key].exclude === undefined) {
        formField.push(
          <Form.Group key={key} controlId={key}>
            <Form.Label>{schema[key].label}</Form.Label>
            <Form.Control
              type={schema[key].type}
              name={key}
              value={this.state[key]}
              onChange={this.handleChange}
              placeholder={schema[key].label}
            />
          </Form.Group>
        );
      }

    });

    return formField;
  };
  render() {
    const { operation, entity, schema } = this.props;

    return (
      <div>
        <h2>
          {operation} {entity}
        </h2>
        <Row>
          <Col sm={7}>
            <Form onSubmit={(e) => this.handleSubmit(e)}>
              <this.ListField />
              <Form.Group>
                <Form.Control type="hidden" name="id" value={this.state.id} />
                <Button variant="success" type="submit">
                  {operation} {entity}
                </Button>
              </Form.Group>
            </Form>
          </Col>
        </Row>
      </div>
    );
  }
}

export default AddNew;
