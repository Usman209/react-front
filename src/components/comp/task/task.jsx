import React from "react";
import { Row, Form, Col, Button } from "react-bootstrap";

import img1 from "../../../assets/images/users/1.jpg";
import img2 from "../../../assets/images/users/2.jpg";
import img3 from "../../../assets/images/users/3.jpg";
import img4 from "../../../assets/images/users/4.jpg";

import axios from "axios";

import {
  Card,
  CardBody,
  CardTitle,
  CardSubtitle,
  Input,
  Table,
} from "reactstrap";
import New from "../../common/New";
import AddNew from "../../common/AddNew";

const Tasks = () => {
  const [companies, setCompanies] = React.useState({});
  const [tasks, setTasks] = React.useState({});
  const [addNew, setAddNew] = React.useState(false);

  const taskForm = {
    entity: "Company",
    operation: "Add",
    schema: {
      id: {
        value: null,
        exclude: true,
      },
      company_name: {
        label: "Company Name",
        type: "text",
        value: "",
      },
      company_email: {
        label: "Company Email",
        type: "text",
        value: "",
      },
      company_website: {
        label: "Company Website",
        type: "text",
        value: "",
      },
      company_address: {
        label: "Company address",
        type: "text",
        value: "",
      },
      company_phone: {
        label: "Company phone",
        type: "text",
        value: "",
      },
    },
  };

  const editTask = {
    id: 1,
    company_name: "Company",
    company_email: "mail@email.com",
    company_website: "website",
    company_address: "address",
    company_phone: "12312321",
  };

  // React.useEffect(() => {
  //   axios
  //     .get("http://localhost:3000/api/company")
  //     .then(({ data }) => setCompanies(data));
  // });

  let newTaskForState = {};

  // React.useEffect(() => {
  //   // setTasks({ ...newTaskForState });
  // }, [tasks]);

  const handleClick = async (task = {}) => {
    task &&
      Object.keys(task).forEach((t) => {
        taskForm.schema[t].value = task[t];
      });

    if (task) taskForm["operation"] = "Edit";

    newTaskForState = await Object.assign({}, taskForm);
    setTasks(newTaskForState);
    setAddNew(true);
  };

  const formSubmitHandler = (form) => {
    const req = {
      Add: "post",
      Edit: "put",
    };

    console.log(newTaskForState);

    console.log(req[newTaskForState["operation"]]);

    axios[req[taskForm.operation]]("/company", form)
      .then((response) => {})
      .catch((err) => {});
  };

  

  return (
    /*--------------------------------------------------------------------------------*/
    /* Used In Dashboard-4 [General]                                                  */
    /*--------------------------------------------------------------------------------*/

    <Card>
      <CardBody>
        <div className="d-flex align-items-center">
          <div>
            <CardTitle>Companies of the Month</CardTitle>
            <CardSubtitle>Overview of Latest Month</CardSubtitle>
          </div>
          <div className="ml-auto d-flex no-block align-items-center">
            <div className="ml-auto d-flex">
              <button
                onClick={() => handleClick({ ...editTask })}
                className="btn btn-primary"
              >
                New
              </button>
              {addNew && (
                <AddNew
                  {...tasks}
                  onFormSubmit={(form) => formSubmitHandler(form)}
                />
              )}
              {/* <New show={addNew} onClickHandle={setAddNew}>

                <form>
                  <input type="text" placeholder="name here" />
                
                  <button className="btn btn-primary">Save</button>
                </form>
              </New> */}
            </div>
            <div className="dl">
              <Input type="select" className="custom-select">
                <option value="0">Monthly</option>
                <option value="1">Daily</option>
                <option value="2">Weekly</option>
                <option value="3">Yearly</option>
              </Input>
            </div>
          </div>
        </div>
        <Table className="no-wrap v-middle" responsive>
          <thead>
            <tr className="border-0">
              <th className="border-0">Company Name</th>
              <th className="border-0">Company Email</th>

              <th className="border-0">Company Address</th>
              <th className="border-0">Company website</th>
              <th className="border-0">Company Phone</th>
            </tr>
          </thead>
          <tbody>
            {/* {companies.length ? (
              companies.map((company) => {
                return (
                  <tr key={company.id}>
                    <td>
                      <div className="d-flex no-block align-items-center">
                        <div className="mr-2">
                          <img
                            src={img1}
                            alt="user"
                            className="rounded-circle"
                            width="45"
                          />
                        </div>
                        <div className="">
                          <h5 className="mb-0 font-16 font-medium">
                            {company.company_name}
                          </h5>
                        </div>
                      </div>
                    </td>

                    <td>
                      <div className="d-flex no-block align-items-center">
                        <div className="mr-2">
                          <img
                            src={img1}
                            alt="user"
                            className="rounded-circle"
                            width="45"
                          />
                        </div>
                        <div className="">
                          <h5 className="mb-0 font-16 font-medium">
                            {company.company_email}
                          </h5>
                        </div>
                      </div>
                    </td>

                    <td>
                      <div className="d-flex no-block align-items-center">
                        <div className="mr-2">
                          <img
                            src={img1}
                            alt="user"
                            className="rounded-circle"
                            width="45"
                          />
                        </div>
                        <div className="">
                          <h5 className="mb-0 font-16 font-medium">
                            {company.company_address}
                          </h5>
                        </div>
                      </div>
                    </td>

                    <td>
                      <div className="d-flex no-block align-items-center">
                        <div className="mr-2">
                          <img
                            src={img1}
                            alt="user"
                            className="rounded-circle"
                            width="45"
                          />
                        </div>
                        <div className="">
                          <h5 className="mb-0 font-16 font-medium">
                            {company.company_website}
                          </h5>
                        </div>
                      </div>
                    </td>

                    <td>
                      <div className="d-flex no-block align-items-center">
                        <div className="mr-2">
                          <img
                            src={img1}
                            alt="user"
                            className="rounded-circle"
                            width="45"
                          />
                        </div>
                        <div className="">
                          <h5 className="mb-0 font-16 font-medium">
                            {company.company_phone}
                          </h5>
                        </div>
                      </div>
                    </td>
                  </tr>
                );
              })
            ) : (
              <tr>
                <td>No Companies Found</td>
              </tr>
            )} */}
          </tbody>
        </Table>
      </CardBody>
    </Card>
  );
};

export default Tasks;
